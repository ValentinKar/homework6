<?php 
mb_internal_encoding('utf-8');  //позволяет использовать все ф-ции для кириллицы
error_reporting(E_ALL);        //вывести на экран все ошибки
$content = file_get_contents("tests.json");    //загружаю json-данные из файла
$tests = json_decode($content, true);     //json-данные записываю в массив

// vvvvvv   Проверяю, передан ли номер теста методом гет
if ($_SERVER['REQUEST_METHOD'] === 'GET' && !empty($_GET['number']) )  {
	$i = 1; 
	foreach ($tests as $k => $a) { 
		if ($i === (integer)$_GET['number']) {  //если номер теста по порядку и переданный номер совпадают
		$theme = $k; // Опрелеляю тему теста, если номер теста передан методом гет
		}
		$i = $i + 1;
	}
}
else { 
	echo 'номер теста не определен';
	exit;  // Если номера теста нет, тогда выхожу из программы
};
// ^^^^^^   Проверяю, передан ли номер теста методом гет
$test = $tests[$theme];   // Выбираю тест, согласно переданному номеру 

$right_all = NULL;   // ввожу переменную, общую для всего теста, опредедяющую правильность выполнения
if (isset($_GET['q_1']))   // если в методе GET передан первый вопрос теста - т.е. если тест выполнен
{ 
	$right_all = true; 
	$i = 1; 
	foreach ($_GET as $quest => $answer) // читаю переданные ответы после выполнения теста  
	{
		if ($quest !== "number") // пропускаю первый ключ массива $_GET т.к. он передает номер, а мне нужны ответы
		{
			if ($test['block_'.$i]['radio'][$answer] === $test['block_'.$i]['answer']) // если переданный номер ответа совпадает с правильным ответом из json
				{ 
					$right[$i] = true;  // индивидуальная переменная для одного вопроса, которая описывает правильность ответа
				}
			else 
				{ 
				$right_all = false;  // общая переменная для всего теста, которая описывает правильность ответа
				$right[$i] = false;  // индивидуальная переменная для одного вопроса, которая описывает правильность ответа
				}; 
			$i++;
		};
	};
}; 
?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>Test</title>
<style>
body {
 font-family: sans-serif;
 font-size: 15px;
}
table {
border: 1px solid black; 
padding: 4px;
}
td {
border: 1px solid black; 
padding: 4px;
}
</style>
</head>
<body>
<h1>Тест: <?php echo $theme; ?></h1>

<?php 
if ($right_all === true) { 
echo "Тест выполнен успешно!"; 
}; 
if ($right_all === false) { 
echo "Тест выполнен неудачно!"; 
}; ?>
<br/><br/><br/>

<form action="test.php" method="GET">
<input type="hidden" name="number" value="<?php echo $_GET['number']; ?>">
<?php $i = 1; foreach ($test as $keys => $array) { ?>
	<?php 
	echo $array['question'];  // Вывожу вопрос в тело страницы
	if (!empty($right[$i]) && $right[$i] === true) { 
	echo " - правильно"; 
	}
	if (isset($right[$i]) && $right[$i] === false) { 
	echo " - неправильно"; 
	}; 
?>
	<br/>
		<?php 
		foreach ($array['radio'] as $key => $answer) 
		{ 
		?>
		<label>
		<input type="radio" name='<?php echo 'q_'.$i; ?>' value='<?php echo $key; ?>'>
		<?php echo $answer; ?>
		</label>
		<br/>
		<?php 
		}; 
		?>
	<br/><br/>
<?php 
$i++; 
}; ?>
<input type="submit" value="Отправить" >
</form>

</body>
</html>