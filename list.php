<?php 
mb_internal_encoding('utf-8');  //позволяет использовать все ф-ции для кириллицы
error_reporting(E_ALL);        //вывести на экран все ошибки
$content = file_get_contents("tests.json");    //загружаю json-данные из файла
$tests = json_decode($content, true);     //json-данные записываю в массив
?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>List</title>
</head>
<body>
<h1>Список тестов: </h1>
<ol>
	<?php 
	foreach ($tests as $key => $array)  
	{ 
	?>
		<li><?php  echo $key; ?></li>
	<?php  
	}; 
	?>
</ol>
</body>
</html>