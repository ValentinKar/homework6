<?php 
mb_internal_encoding('utf-8');  //позволяет использовать все ф-ции для кириллицы
error_reporting(E_ALL);        //вывести на экран все ошибки
$cont = file_get_contents("tests.json");    //загружаю json-данные из файла
$t = json_decode($cont, true);     //json-данные записываю в массив

// vvvvv Проверяю, была ли загрузка нового теста методом POST и был ли передан первый вопрос 
if ($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST['question_1']) )  {
	foreach ($t as $k1 => $v) {
		if ($k1 === $_POST['theme']) {  // Проверяю, нет ли совпадений темы нового теста и тем тестов из json файла
			echo 'ТЕМА: '.$_POST['theme'].' уже есть - надо изменить тему или добавить номер'; 
			exit; 
		}
	}
for ($i = 1; $i < 5; $i++) {
$answer = "";  // Переменная сохраняющая правильный ответ 
if (isset($_POST['correct_'.$i]) && $_POST['correct_'.$i] === '1') {
$answer = htmlspecialchars($_POST['answer_'.$i.'1']);   // Правильный ответ 
}
if (isset($_POST['correct_'.$i]) && $_POST['correct_'.$i] === '2') {
$answer = htmlspecialchars($_POST['answer_'.$i.'2']);   // Правильный ответ 
}
if (isset($_POST['correct_'.$i]) && $_POST['correct_'.$i] === '3') {
$answer = htmlspecialchars($_POST['answer_'.$i.'3']);   // Правильный ответ 
}
if (isset($_POST['correct_'.$i]) && $_POST['correct_'.$i] === '4') {
$answer = htmlspecialchars($_POST['answer_'.$i.'4']);   // Правильный ответ 
}

$arr["block_".$i] = [  // Создаю новый массивчик для вопроса с вариантами ответов
	"question" => htmlspecialchars($_POST['question_'.$i]),  // вопрос
	"answer" => $answer,      // правильный ответ
		"radio" => [
		htmlspecialchars($_POST['answer_'.$i.'1']),  // 1-ый вариант ответа
		htmlspecialchars($_POST['answer_'.$i.'2']),  // 2-ый вариант ответа 
		htmlspecialchars($_POST['answer_'.$i.'3']),  // 3-ый вариант ответа 
		htmlspecialchars($_POST['answer_'.$i.'4'])   // 4-ый вариант ответа 
		]
	];

if ($answer === "") {   // если правильный ответ не определен
	echo 'не определен правильный ответ в блоке '.$i; 
	exit; 
}
};

$array_new = [  // Создаю общий массивчик для теста, в который записываю массивчики для вопросов
$_POST['theme'] => [            // тема теста
'block_1' => $arr['block_1'],   // 1-ый вопрос
'block_2' => $arr['block_2'],   // 1-ый вопрос
'block_3' => $arr['block_3'],   // 1-ый вопрос
'block_4' => $arr['block_4']    // 1-ый вопрос
]
];
$array = array_merge($t, $array_new);  // обьединяю массив из json файла и массивчик с новым тестом
$content = json_encode($array,JSON_UNESCAPED_UNICODE);  // кодирую массив, полученный в предыдущей строке в json формат
file_put_contents("tests.json", $content);     // записываю результат в json файл
echo "тест добавлен в файл tests.json";
exit; 
}
// ^^^^^ Проверяю, была ли загрузка нового теста методом POST и был ли передан первый вопрос 
?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>Load test</title>
<style>
body {
 font-family: sans-serif;
 font-size: 15px;
}
.block {
border: 1px solid black; 
padding: 10px;
margin: 10px;
width: 900px;
}
</style>
</head>
<body>

Темы тестов, которые уже есть на сайте (дословно не повторять, можно добавлять номер):
<br/><br/>
<?php 
foreach ($t as $k => $a) 
{ 
echo $k; 
?>
<br/>
<?php 
}; 
?>
<br/>
<h1>Загрузить на сервер следующий тест:</h1>
<form action="admin.php" method="post" enctype="multipart/form-data">
<input type="text" name="theme" size="80" placeholder="тема теста" value="тема_1" >
<?php 
for ($i = 1; $i < 5; $i++) :  
?>
<div class="block">
Блок №<?php echo $i; ?>: (вопрос с вариантами ответа) <br/><br/>
<input type="text" name="question_<?php echo $i; ?>" size="120" placeholder="вопрос" value="вопрос<?php echo $i; ?>" >
<br/><br/><hr><br/>
<input type="radio" name="correct_<?php echo $i; ?>" value="1">
<input type="text" name="answer_<?php echo $i; ?>1" size="100" placeholder="вариант ответа" value="ответ<?php echo $i; ?>1" >
<br/><br/>
<input type="radio" name="correct_<?php echo $i; ?>" value="2">
<input type="text" name="answer_<?php echo $i; ?>2" size="100" placeholder="вариант ответа" value="ответ<?php echo $i; ?>2" >
<br/><br/>
<input type="radio" name="correct_<?php echo $i; ?>" value="3">
<input type="text" name="answer_<?php echo $i; ?>3" size="100" placeholder="вариант ответа" value="ответ<?php echo $i; ?>3" >
<br/><br/>
<input type="radio" name="correct_<?php echo $i; ?>" value="4">
<input type="text" name="answer_<?php echo $i; ?>4" size="100" placeholder="вариант ответа" value="ответ<?php echo $i; ?>4" >
<br/>
<br/>
Поставьте переключатель напротив правильного ответа!
</div>
<?php 
endfor;  
?>
<input type="submit" value="Загрузить на сервер">
</form>

</body>
</html>